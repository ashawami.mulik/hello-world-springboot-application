FROM openjdk:8-jdk-alpine
RUN addgroup -S ashawami && adduser -S ashawami -G ashawami
USER ashawami:ashawami
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]